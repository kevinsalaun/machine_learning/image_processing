### PARAMETERS ###
root_path = '../data'
train_path = f'{root_path}/db_train/'
valid_path = f'{root_path}/db_valid/'
test_path = f'{root_path}/db_test/'
test_res_path = f'{root_path}/db_test_res/'
