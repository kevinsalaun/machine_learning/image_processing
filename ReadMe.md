Le rapport contenant le compte-rendu du projet et les explications sur l'architecture de ce dépôt est disponible dans le dossier "docs".

Pour un meilleur confort de lecture, préféré la version Markdown (.md), certaines images n'apparaissant pas dans la version PDF.
