ifeq ($(OS),Windows_NT)
	PYTHON=python3.exe
else
	PYTHON=python3
endif
REQUIREMENTS_FILE=requirements
ifneq (,$(wildcard $(REQUIREMENTS_FILE)))
	INSTALL_REQ=.env/bin/pip install -r $(REQUIREMENTS_FILE)
else
	INSTALL_REQ=echo "The \"$(REQUIREMENTS_FILE)\" file doesn't exist"
endif

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


install: ## install module in virtualenv
	virtualenv .env -p python3
	$(INSTALL_REQ)

install_req:
	$(INSTALL_REQ)
