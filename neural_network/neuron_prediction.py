from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model, model_from_json
from keras.utils.np_utils import to_categorical #probas_to_classes
from keras.preprocessing import image
from os.path import exists, isfile, join, basename
from shutil import copyfile, rmtree
from os import listdir, makedirs
import tqdm
import sys
import numpy as np
import params
import json

def makePredictions(backup_folder):
    ### CNN MODEL ###
    print(f'model files loading...')

    categories_file = f'{backup_folder}/categories.json'
    architecture_file = f'{backup_folder}/architecture.json'
    weights_file = f'{backup_folder}/weights.h5'

    categories = None
    model = None
    with open(categories_file) as f:
        categories = json.load(f)
    with open(architecture_file, 'r') as f:
        model_json = json.load(f)
        model = model_from_json(model_json)
        model.load_weights(weights_file)
    model.summary()

    ### PREDICTION ###
    if exists(params.test_res_path): rmtree(params.test_res_path)
    files_path = []
    files_data = []
    category_folders = listdir(params.test_path)
    for d in category_folders:
        category_folder = join(params.test_path, d)
        for f in listdir(category_folder):
            if f.endswith(".jpg"):
                src = join(category_folder, f)
                test_image = image.load_img(src, target_size=params.resize)
                test_image = image.img_to_array(test_image)
                test_image = np.expand_dims(test_image, axis=0)
                files_path.append(src)
                files_data.append(test_image)
    progressbar = tqdm.tqdm(total=len(files_data))
    for idx, f in enumerate(files_data):
        filename = basename(files_path[idx])
        predict_idx = model.predict_classes(f, batch_size=1)[0]
        predict_category = list(categories.keys())[list(categories.values()).index(predict_idx)]
        target_dir = join(str(params.test_res_path), str(predict_category))
        makedirs(target_dir, exist_ok=True)
        dst = join(target_dir, filename)
        #print(f'copy {files_path[idx]} to {dst}')
        copyfile(files_path[idx], dst)
        progressbar.update(1)

    print('Classification performed !!')

    # evaluates the accuracy
    evaluation = {}
    for d in category_folders:
        evaluation[d] = {
            'TP': 0,
            'FN': 0,
            'FP': 0,
            'accuracy': 0,
            'recall': 0
        }
        category_folder = join(params.test_path, d)
        category_folder_res = join(params.test_res_path, d)
        for f in listdir(category_folder):
            if f.endswith(".jpg"):
                #original = join(category_folder, f)
                categorized = join(category_folder_res, f)
                if exists(categorized):
                    evaluation[d]['TP'] += 1
                else:
                    evaluation[d]['FN'] += 1
        if exists(category_folder_res):
            for f in listdir(category_folder_res):
                if f.endswith(".jpg"):
                    #categorized = join(category_folder_res, f)
                    original = join(category_folder, f)
                    if not exists(original):
                        evaluation[d]['FP'] += 1
        if evaluation[d]['TP']:
            evaluation[d]['accuracy'] = evaluation[d]['TP'] / (evaluation[d]['TP'] + evaluation[d]['FP'])
            evaluation[d]['recall'] = evaluation[d]['TP'] / (evaluation[d]['TP'] + evaluation[d]['FN'])
        print(f'"{d}" evaluation: {evaluation[d]}')
       

def main():
    if len(sys.argv) < 2:
        print('Please enter the backup folder name (contains json and h5 files)')
        exit(1)
    backup_folder = sys.argv[1]
    makePredictions(backup_folder)

if __name__ == "__main__":
    main()
