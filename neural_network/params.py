### PARAMETERS ###
# batch_size = N randomly selected images from across the classes in the dataset will be returned in each batch when training
# resize = resize pictures before evaluate
root_path = '../data'
train_path = f'{root_path}/db_train/'
valid_path = f'{root_path}/db_valid/'
test_path = f'{root_path}/db_test/'
test_res_path = f'{root_path}/db_test_res/'

result_data_path = f'result_data'
h5_file_path = f'{result_data_path}/<filename>.h5'
json_file_path = f'{result_data_path}/<filename>.json'
png_file_path = f'{result_data_path}/<filename>.png'

#batch_size = 50
batch_size = 5
resize = (150, 150)
#steps_per_epoch = 2000 // batch_size
epochs = 15
#validation_steps = 800 // batch_size
#validation_steps = 40

inputShape = (150, 150, 3)
kernel_size = (3, 3)
pool_size = (2, 2)
