from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.callbacks import EarlyStopping, ModelCheckpoint
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import params
import json


def model_classifier(params, category_count):
    print('Model "model_classifier" is selected')

    # entry points
    model = Sequential()
    
    model.add(Conv2D(filters=32, kernel_size=params.kernel_size, activation='relu', input_shape=params.inputShape))
    model.add(MaxPooling2D(pool_size=params.pool_size)) # extract the maximum value of a 2x2 mask
    model.add(Dropout(0.25))

    model.add(Conv2D(filters=64, kernel_size=params.kernel_size, activation='relu'))
    #model.add(Dropout(0.25))
    model.add(MaxPooling2D(pool_size=params.pool_size))

    model.add(Conv2D(filters=64, kernel_size=params.kernel_size, activation='relu'))
    model.add(MaxPooling2D(pool_size=params.pool_size))
    model.add(Dropout(0.25))

    # converts 3D feature maps to 1D feature vectors
    model.add(Flatten())

    # fully-connected layer (beginning of multilayer perceptron)
    model.add(Dense(64, activation='relu'))

    # randomly turn off 30% of neurons
    #model.add(Dropout(0.5))

    # Last layer (add number of layers based on number of categories)
    model.add(Dense(category_count, activation='softmax'))

    model.compile(
        loss='sparse_categorical_crossentropy', # categorical_crossentropy
        optimizer='adam', # 'rmsprop'
        metrics=['accuracy']
    )

    return model


def save_perf(model, history, epochs, backup_folder):
    architecture_file = f'{backup_folder}/architecture.json'
    weights_file = f'{backup_folder}/weights.h5'
    training_file = f'{backup_folder}/training.png'
    
    model.save_weights(weights_file)
    with open(architecture_file, "w") as json_file:
        json.dump(model.to_json(), json_file)

    # plot the training loss and accuracy
    interval = np.arange(0, len(history.history["loss"]))  #epochs)
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(interval, history.history["loss"], label="train_loss")
    plt.plot(interval, history.history["val_loss"], label="val_loss")
    plt.plot(interval, history.history["accuracy"], label="train_acc")
    plt.plot(interval, history.history["val_accuracy"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend(loc="lower left")
    plt.savefig(training_file)

    # save filter rendering + confusing matrix
    # config = model.get_config()
    # TODO: ...


def startTraining():
    #### CNN MODEL ###
    # create a data generator
    # rescale for robust to scale changes
    train_datagen = ImageDataGenerator(rescale=1./255)
    val_datagen = ImageDataGenerator(rescale=1./255)

    # load and iterate training, validation and test datasets
    train_it = train_datagen.flow_from_directory(
        directory=params.train_path, 
        target_size=params.resize, 
        class_mode='sparse',  #categorical
        batch_size=params.batch_size, 
        shuffle=True
    )

    val_it = val_datagen.flow_from_directory(
        directory=params.valid_path, 
        target_size=params.resize, 
        class_mode='sparse',  #categorical
        batch_size=params.batch_size
    )

    categories = train_it.class_indices
    print('categories:', categories)

    # load model
    model = model_classifier(params, len(categories))

    # fit model 
    #checkpoint_callback = ModelCheckpoint(filename_path, monitor='val_loss', verbose=2, save_best_only=True, mode='min')
    early_stopping_callback = EarlyStopping(monitor='val_loss', patience=3)
    steps_per_epoch = train_it.n//train_it.batch_size
    validation_steps = val_it.n//val_it.batch_size
    history = model.fit_generator(
        train_it,
        epochs=params.epochs,
        steps_per_epoch=steps_per_epoch,
        validation_data=val_it,
        validation_steps=validation_steps,
        verbose=2,
        callbacks=[early_stopping_callback] # checkpoint_callback
    )

    # save model and weights
    backup_folder = f'{params.result_data_path}/{datetime.now().isoformat()}'  #.strftime('%b-%d-%I%M%p-%G')
    os.makedirs(backup_folder, exist_ok=True)

    categories_file = f'{backup_folder}/categories.json'
    with open(categories_file, 'w') as f:
        json.dump(categories, f)
    save_perf(model, history, params.epochs, backup_folder)

    return backup_folder


def main():
    startTraining()

if __name__ == "__main__":
    main()
