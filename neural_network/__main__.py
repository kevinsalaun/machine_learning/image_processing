from neuron_train import startTraining
from neuron_prediction import makePredictions

def main():
    backup_folder = startTraining()
    makePredictions(backup_folder)

if __name__ == "__main__":
    main()
