#!/usr/bin/python
# -*- coding: utf-8 -*-

from os import listdir, mkdir
from os.path import exists, isdir, isfile, join, splitext
from shutil import copyfile, rmtree

global root, feature_number
global path_training, path_test, path_validation

root = 'database'
feature_number = 5

def to_int(s):
    try: 
        return int(s)
    except ValueError:
        return None


def create_folders():
    global path_training, path_test, path_validation

    path_training = 'db_train'
    path_test = 'db_test'
    path_validation = 'db_valid'

    if not exists(path_training): mkdir(path_training)
    if not exists(path_test): mkdir(path_test)
    if not exists(path_validation): mkdir(path_validation)


def create_features(path):
    feature_training = join(path_training, path)
    feature_validation = join(path_validation, path)

    if exists(feature_training): rmtree(feature_training)
    if exists(feature_validation): rmtree(feature_validation)
    mkdir(feature_training)
    mkdir(feature_validation)


def main():
    create_folders()

    feature_count = 1
    for feature in listdir(root):
        root_feature = join(root, feature)
        if feature_count <= feature_number and isdir(root_feature):
            feature_count += 1
            create_features(feature)
            for picture in listdir(root_feature):
                root_picture = join(root_feature, picture)
                if isfile(root_picture):
                    filename = splitext(picture)[0]
                    file_last_int = to_int(filename[len(filename)-1])
                    if file_last_int != None:
                        rel_path = join(feature, picture)
                        src = join(root, rel_path)
                        dst = ""
                        if file_last_int %3 == 0:
                            dst = join(path_training, rel_path)
                        elif file_last_int %2 == 0:
                            dst = join(path_validation, rel_path)
                        else:
                            dst = join(path_test, rel_path) #picture)
                        copyfile(src, dst)


if __name__ == "__main__":
    main()
