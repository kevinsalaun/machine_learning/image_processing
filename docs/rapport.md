<style type='text/css'>
img[src*='#left'] { 
    float: left;
}
img[src*='#clear'] { 
    clear: both;
}
</style>

# Projet Traitement et Analyse d’Images
## Kévin Salaün - IMR 3


## Sommaire
I. Préparation des données

II. Classification par attributs (old school)
 * Adaptation CBIR pour classification
 * Méthode d'évaluation
 * Extracteurs d'attributs
   * A) Histogramme RGB
   * B) Histogramme de gradient orienté (HOG)
   * C) Filtre de Gabor
   * D) Fusion 
     * D.1) Histogramme RGB + Histogramme de gradient orienté
     * D.2) Histogramme de gradient orienté + Filtre de Gabor
     * D.3) Histogramme RGB + Histogramme de gradient orienté + Filtre de Gabor
 * Conclusion intermédiaire

III. Convolutional Neural Network (new school)
 * CNN
 * Préparation des lots d'entraînement
 * Modèle
 * Résultats
    * A) Entraînement
    * B) Prédictions
 * Conclusion intermédiaire

Conclusion générale

## Prérequis
Afin d'utiliser le projet, suivez la démarche suivante:
```bash
git clone https://gitlab.com/kevinsalaun/machine_learning/image_processing
cd image_processing

# Option 1: utilisation du makefile
make

# Option 2: installation manuelle
virtualenv .env -p python3
source .env/bin/activate
pip install -r requirements
```

Le dossier "classification" contient la partie classification par attributs, le dossier "neural_network" le CNN. Le dossier "data" contient les donnés ainsi que le résultat des images rangées selon les prédictions (sous-dossier "db_test_res").

## I. Préparation des données
La base COREL comprend 10800 images regroupées en 80  groupes  thématiques,  pour  une  taille  totale  de  40  Mo. Cette base constitue notre dataset.  Les  images  sont  de  petites dimensions (80x120 ou 120x80), ce qui permet de réduire les temps de traitement. Notre dataset d’entrée est donc un ensemble de fichiers images classifiées en dossier comme représenté ci-dessous :

```
data
│
└─── art_1
│   │   193000.jpg
│   │   193003.jpg
│   │   ...
│
└─── art_antiques
│   │   ...
│   
└─── art_cybr
|   │   ...
|
└─── ...
```

Afin de réaliser les différents traitements, j'ai dans un premier temps réalisé un script python (dispatcher.py) permettant de répartir équitablement les données de chaque catégorie en trois jeux de données :
  * train -> la classification des images est connue, le système de classification apprend à dissocier les types d'images en ajustant des poids
  * valid -> le système prédit une classification pour chaque image et vérifie si elle correspond à la classification réelle (évaluation du modèle)
  * test -> le système réalise une prédiction pour un lot d'images dont la classification est inconnue et range les fichiers images dans les catégories prédites
  
Le script "dispatcher.py" s'utilise de la manière suivante :
```bash
python dispatcher.py
```

## II. Classification par attributs (old school)
Pour cette partie, nous nous sommes basé sur le code fourni par le projet [CBIR](https://github.com/pochih/CBIR/) (Content-Based Image Retrieval). Ce projet contient un ensemble d'extracteurs d'attributs (méthode "handcrafted features") basés sur des caractéristiques telles que la couleur (histogramme RGB), la texture (filtre gabor), la forme (daisy, histogramme des bordures, histogramme de gradient ordonné).

Je me suis particulièrement intéressé à trois extracteurs : l'histogramme RGB (color.py), l'histogramme de gradient orienté (HOG.py) et le filtre Gabor (gabor.py).


### Adaptation CBIR pour classification
Le projet CBIR a été initialement conçu pour retrouver les images  les  plus  proches  d’une  requête, autrement dit, cherche parmi les images lesquelles partages une série d'attributs. J'ai donc dû adapter le code pour modifier le résultat de chaque requête afin d'attribuer une (unique) classe prédite, basé sur le mode pondéré (weighted mode) de la réponse à la requête. Cette mutation du code a été réalisée dans la méthode "infer" du fichier "evaluate.py".

### Méthode d'évaluation
Avant de décrire les modèles et leurs résultats, il est important de définir la méthode d'évaluation utilisée.
Le système CBIR récupère les images en fonction de la similarité des caractéristiques. La robustesse du système est évaluée par le MMAP (mean MAP), les formules d'évaluation sont mentionnées [ici](http://web.stanford.edu/class/cs276/handouts/EvaluationNew-handout-1-per.pdf).

 * image AP : moyenne de la précision à chaque coup
   * profondeur = K, cela signifie que le système retournera des images top-K
   * une image correcte dans le top-K est appelée un hit
   * AP = (hit1.precision + hit2.precision + ... + hitH.precision) / H
 * class1 MAP = (class1.img1.AP + class1.img2.AP + ... + class1.imgM.AP) / M
 * MMAP = (class1.MAP + class2.MAP + ... + classN.MAP) / N

### Extracteurs d'attributs
#### A) Histogramme RGB
L'histogramme de couleurs est un graphique statistique qui utilise comme abscisse des couleurs et pour ordonnées le nombre de pixels pour chaque couleur, ce qui permet d'évaluer rapidement la distribution des couleurs dans une image. Dans le cas de l'histogramme RGB, trois histogrammes de couleurs sont réalisés, un pour chaque couleur : rouge, vert, bleu.

![alt text](./images/rgb.jpg "Histogramme RGB")

Cet extracteur est défini dans le fichier color.py, il possède deux modes de fonctionnement, le premier, "global", réalise l'histogramme sur l'ensemble de l'image, le second, "region", concatène des histogrammes réalisés pour chaque région (fragment) de l'image.

La classification obtenue est la suivante :

art_1 (4)

![my image](./images/results/color/art_1/283057.jpg#left)
![my image](./images/results/color/art_1/283061.jpg#left)
![my image](./images/results/color/art_1/283065.jpg#left)
![my image](./images/results/color/art_1/283071.jpg#clear)

art_antiques (0)

art_cybr (78)

![my image](./images/results/color/art_cybr/193001.jpg#left)
![my image](./images/results/color/art_cybr/193005.jpg#left)
![my image](./images/results/color/art_cybr/193007.jpg#left)
![my image](./images/results/color/art_cybr/193011.jpg#clear)

art_dino (40)

![my image](./images/results/color/art_dino/193015.jpg#left)
![my image](./images/results/color/art_dino/193025.jpg#left)
![my image](./images/results/color/art_dino/193037.jpg#left)
![my image](./images/results/color/art_dino/193047.jpg#clear)

art_mural (6)

![my image](./images/results/color/art_mural/283031.jpg#left)
![my image](./images/results/color/art_mural/435031.jpg#left)
![my image](./images/results/color/art_mural/435075.jpg#left)
![my image](./images/results/color/art_mural/435097.jpg#clear)

```
Nombre d'images : 128
Class art_1, MAP 0.925
Class art_mural, MAP 0.95
Class art_antiques, MAP 0.975
Class art_cybr, MAP 0.9875
Class art_dino, MAP 1.0
**MMAP 0.9675**
```

Méthode d'utilisation :
```bash
  python color.py
```

#### B) Histogramme de gradient orienté (HOG)
HOG est une technique de détection de contours. Les contours d’une image sont des structures linéaires marquant les frontières entre objets distincts. Ils correspondent généralement à des transitions plus ou moins rapides de niveau de gris ou de couleur. La particularité du gradient orienté est qu'il détecte un contour par rapport à l'orientation des couleurs comme on peut le voir sur la figure ci-dessous :

![alt text](./images/hog_2.png "HOG")

HOG représente une méthode de classification fiable qui capture les similitudes entre les objets de la même classe et les différences avec des objets de classes concurrentes en se basant sur l’apparence de l’objet. L'usage de gradient à l'avantage de mieux représenter la structure interne d’un objet, permettant ainsi de surmonter les problèmes liés à l'apparence de l'objet : pose, éclairage, occlusion, texture de fond...

La classification obtenue est la suivante :

art_1 (2)

![my image](./images/results/hog/art_1/435075.jpg#left)
![my image](./images/results/hog/art_1/554065.jpg#clear)

art_antiques (14)

![my image](./images/results/hog/art_antiques/193041.jpg#left)
![my image](./images/results/hog/art_antiques/283001.jpg#left)
![my image](./images/results/hog/art_antiques/283011.jpg#left)
![my image](./images/results/hog/art_antiques/283017.jpg#clear)

art_cybr (56)

![my image](./images/results/hog/art_cybr/193001.jpg#left)
![my image](./images/results/hog/art_cybr/193011.jpg#left)
![my image](./images/results/hog/art_cybr/193017.jpg#left)
![my image](./images/results/hog/art_cybr/193021.jpg#clear)

art_dino (41)

![my image](./images/results/hog/art_dino/193015.jpg#left)
![my image](./images/results/hog/art_dino/193025.jpg#left)
![my image](./images/results/hog/art_dino/193037.jpg#left)
![my image](./images/results/hog/art_dino/193047.jpg#clear)

art_mural (15)

![my image](./images/results/hog/art_mural/193007.jpg#left)
![my image](./images/results/hog/art_mural/283031.jpg#left)
![my image](./images/results/hog/art_mural/435031.jpg#left)
![my image](./images/results/hog/art_mural/435097.jpg#clear)

```
Nombre d'images : 128
Class art_cybr, MAP 0.9375
Class art_antiques, MAP 1.0
Class art_1, MAP 0.675
Class art_dino, MAP 1.0
Class art_mural, MAP 0.9
**MMAP 0.9025**
```

Méthode d'utilisation :
```bash
  python HOG.py
```

#### C) Filtre de Gabor
Le filtre de Gabor est un filtre linéaire dont le noyau est un gaussien 2D modulé par une onde cosinusoïdale. Il est ici utilisé pour la détection de textures.

![alt text](./images/gabor.png "Filtre de Gabor")

La classification obtenue est la suivante:

art_1 (7)

![my image](./images/results/gabor/art_1/193071.jpg#left)
![my image](./images/results/gabor/art_1/193077.jpg#left)
![my image](./images/results/gabor/art_1/283057.jpg#left)
![my image](./images/results/gabor/art_1/283065.jpg#clear)

art_antiques (15)

![my image](./images/results/gabor/art_antiques/193005.jpg#left)
![my image](./images/results/gabor/art_antiques/193017.jpg#left)
![my image](./images/results/gabor/art_antiques/193021.jpg#left)
![my image](./images/results/gabor/art_antiques/193031.jpg#clear)

art_cybr (51)

![my image](./images/results/gabor/art_cybr/193001.jpg#left)
![my image](./images/results/gabor/art_cybr/193007.jpg#left)
![my image](./images/results/gabor/art_cybr/193027.jpg#left)
![my image](./images/results/gabor/art_cybr/193035.jpg#clear)

art_dino (48)

![my image](./images/results/gabor/art_dino/193015.jpg#left)
![my image](./images/results/gabor/art_dino/193025.jpg#left)
![my image](./images/results/gabor/art_dino/193037.jpg#left)
![my image](./images/results/gabor/art_dino/193047.jpg#clear)

art_mural (7)

![my image](./images/results/gabor/art_mural/193011.jpg#left)
![my image](./images/results/gabor/art_mural/193075.jpg#left)
![my image](./images/results/gabor/art_mural/283061.jpg#left)
![my image](./images/results/gabor/art_mural/435075.jpg#clear)

```
Nombre d'images : 128
Class art_1, MAP 0.375
Class art_dino, MAP 0.95
Class art_cybr, MAP 0.6125
Class art_antiques, MAP 0.55
Class art_mural, MAP 0.575
**MMAP 0.6125**
```

Méthode d'utilisation :
```bash
  python gabor.py
```

#### D) Fusion 
Comme on a pu le voir, une image peut être qualifiée par un ensemble d'attributs, mais un attribut seul ne suffit pas à catégoriser une image. Le script "fusion.py" permet donc de prendre en compte un ensemble d'attributs pour fournir une prédiction.


Méthode d'utilisation :
Sélectionner, au travers de la variables "feat_pools", les extracteurs d'attributs à intégrer, puis faites :
```bash
  python fusion.py
```

##### D.1) Histogramme RGB + Histogramme de gradient orienté
Voici les résultats obtenus en cumulant détection de couleurs et de bordures:

art_1 (0)

art_antiques (0)

art_cybr (80)

![my image](./images/results/color_hog_gabor/art_cybr/193001.jpg#left)
![my image](./images/results/color_hog_gabor/art_cybr/193005.jpg#left)
![my image](./images/results/color_hog_gabor/art_cybr/193007.jpg#left)
![my image](./images/results/color_hog_gabor/art_cybr/193011.jpg#clear)

art_dino (40)

![my image](./images/results/color_hog_gabor/art_dino/193015.jpg#left)
![my image](./images/results/color_hog_gabor/art_dino/193025.jpg#left)
![my image](./images/results/color_hog_gabor/art_dino/193037.jpg#left)
![my image](./images/results/color_hog_gabor/art_dino/193047.jpg#clear)

art_mural (8)

![my image](./images/results/color_hog_gabor/art_mural/283057.jpg#left)
![my image](./images/results/color_hog_gabor/art_mural/283061.jpg#left)
![my image](./images/results/color_hog_gabor/art_mural/283065.jpg#left)
![my image](./images/results/color_hog_gabor/art_mural/435031.jpg#clear)

```
Nombre d'images : 128
Class art_mural, MAP 0.975
Class art_cybr, MAP 1.0
Class art_antiques, MAP 1.0
Class art_dino, MAP 1.0
Class art_1, MAP 1.0
**MMAP 0.9949**
```

##### D.2) Histogramme de gradient orienté + Filtre de Gabor
Ci-dessous les résultats obtenus en cumulant HOG et filtre de Gabor:

art_1 (6)

![my image](./images/results/hog_gabor/art_1/193077.jpg#left)
![my image](./images/results/hog_gabor/art_1/283075.jpg#left)
![my image](./images/results/hog_gabor/art_1/435031.jpg#left)
![my image](./images/results/hog_gabor/art_1/435065.jpg#clear)

art_antiques (16)

![my image](./images/results/hog_gabor/art_antiques/193001.jpg#left)
![my image](./images/results/hog_gabor/art_antiques/193005.jpg#left)
![my image](./images/results/hog_gabor/art_antiques/193035.jpg#left)
![my image](./images/results/hog_gabor/art_antiques/193081.jpg#clear)

art_cybr (48)

![my image](./images/results/hog_gabor/art_cybr/193011.jpg#left)
![my image](./images/results/hog_gabor/art_cybr/193017.jpg#left)
![my image](./images/results/hog_gabor/art_cybr/193031.jpg#left)
![my image](./images/results/hog_gabor/art_cybr/193041.jpg#clear)

art_dino (41)

![my image](./images/results/hog_gabor/art_dino/193015.jpg#left)
![my image](./images/results/hog_gabor/art_dino/193025.jpg#left)
![my image](./images/results/hog_gabor/art_dino/193037.jpg#left)
![my image](./images/results/hog_gabor/art_dino/193047.jpg#clear)

art_mural (17)

![my image](./images/results/hog_gabor/art_mural/193007.jpg#left)
![my image](./images/results/hog_gabor/art_mural/193021.jpg#left)
![my image](./images/results/hog_gabor/art_mural/193027.jpg#left)
![my image](./images/results/hog_gabor/art_mural/193075.jpg#clear)


```
Nombre d'images : 128
Class art_antiques, MAP 1.0
Class art_dino, MAP 1.0
Class art_1, MAP 1.0
Class art_mural, MAP 1.0
Class art_cybr, MAP 1.0
MMAP 1.0
```

##### D.3) Histogramme RGB + Histogramme de gradient orienté + Filtre de Gabor
Même résultat que lors de l'expérience D.1, il ne suffit pas de cumuler les extracteurs d'attributs pour obtenir de meilleurs résultats. Les résultats du filtre de Gabor étant nettement inférieur à ceux des histogrammes RGB et de gradient orienté, ont provoqués l'exclusion de cet extracteur. Afin d'améliorer nos résultats, il serait nécessaire de revoir notre manière d'ajuster les poids.

### Conclusion intermédiaire
Hormis quelques erreurs de classement que l'on peut constater dans les aperçus d'images présents pour chaque catégorie, le score MMAP prouve que les prédictions effectuées suite à la mise en corrélation de plusieurs extracteurs d'attributs est généralement stable (résilience à la diversité de contenu du jeu de données) et plus précise que lors de l'usage d'un seul extracteur.


## III. Convolutional Neural Network (new school)
### CNN
Les réseaux de neurones convolutifs (CNN) représentent la technologie montante dans le domaine de la reconnaissance d'images.

Les CNN se décomposent en trois parties :

La première, la partie convolutive, fonctionne comme un extracteur de caractéristiques des images. Une image est passée à travers une succession de filtres, ou noyaux de convolution, créant de nouvelles images appelées cartes de convolutions. Certains filtres intermédiaires réduisent la résolution de l’image par une opération de maximum local (MaxPooling2D). Pour finir, les cartes de convolutions sont mises à plat et concaténées en un vecteur de caractéristiques (Flatten), appelé code CNN.

Ce code CNN en sortie de la partie convolutive est ensuite branché en entrée d’une deuxième partie, constituée de couches entièrement connectées (perceptron multicouche). Le rôle de cette partie est de combiner les caractéristiques du code CNN pour classer l’image.

La sortie est une dernière couche comportant un neurone par catégorie. Les valeurs numériques obtenues sont alors normalisées entre 0 et 1 pour produire une distribution de probabilité sur les catégories.

### Préparation des lots d'entraînement
Dans le cadre du CNN, la qualité des lots d'entraînement représente la pierre angulaire d'un modèle robuste et précis. Dans notre cas, le jeu de données n'était pas assez important pour réaliser un bon entraînement, j'ai donc utilisé un générateur jouant sur l'échelle des images pour augmenter la taille de mon dataset. J'ai également fait le choix de mélanger le jeu d'entraînement pour que le modèle aprène des caractéristiques et surtout pas l'ordre des données.

```bash
# redimensionnement pour la robustesse aux changements d'échelle
train_datagen = ImageDataGenerator(rescale=1./255)

# chargement puis itération apprentissage
train_it = train_datagen.flow_from_directory(
    directory=params.train_path,  # dossier contenant les images d'entraînement
    target_size=params.resize,  # homogénisation de la taille des images
    class_mode='sparse',  # format de matrice optimisé (compression des valeurs nulles)
    batch_size=params.batch_size,  # la taille des lots de données
    shuffle=True  # modifie l'ordre des images étudiées
)
```

### Modèle
Mon modèle est le suivant :

![alt text](./images/CNN_analyse_image.png "CNN")

Ci-dessous le code le décrivant :
```bash
# paramètres
inputShape = (150, 150, 3)
kernel_size = (3, 3)
pool_size = (2, 2)

# point d'entrée
model = Sequential()

model.add(Conv2D(filters=32, kernel_size=params.kernel_size, activation='relu', input_shape=params.inputShape))
# extrait la valeur maximale d'un masque 2x2
model.add(MaxPooling2D(pool_size=params.pool_size))
# désactive aléatoirement une partie du réseau neuronal (ignore certaines entrées)
model.add(Dropout(0.25))

model.add(Conv2D(filters=64, kernel_size=params.kernel_size, activation='relu'))
model.add(MaxPooling2D(pool_size=params.pool_size))

model.add(Conv2D(filters=64, kernel_size=params.kernel_size, activation='relu'))
model.add(MaxPooling2D(pool_size=params.pool_size))
model.add(Dropout(0.25))

# convertit les cartes de caractéristiques 3D en vecteurs de caractéristiques 1D
model.add(Flatten())

# couche entièrement connectée (début du perceptron multicouche)
model.add(Dense(64, activation='relu'))

# Dernière couche (autant de couches que de catégories)
model.add(Dense(category_count, activation='softmax'))

model.compile(
    loss='sparse_categorical_crossentropy',
    optimizer='adam',
    metrics=['accuracy']
)
```

### Résultats
Méthode d'utilisation :
```bash
  # Lancer l'apprentissage puis les prédictions
  python __main__.py

  # Lancer seulement l'apprentissage
  python neuron_train.py

  # Lancer seulement les prédictions
  python neuron_prediction.py <fichier.h5>
```

L'architecture du modèle ainsi que les poids calculés durant l'entraînement sont stockés dans le dossier "neural_network/result_data".

#### A) Entraînement
L'apprentissage a été réalisé via le code suivant :
```python
# paramètres
batch_size = 5
resize = (150, 150)
epochs = 15
steps_per_epoch = train_it.n//train_it.batch_size
validation_steps = val_it.n//val_it.batch_size

# apprentissage
early_stopping_callback = EarlyStopping(monitor='val_loss', patience=3)
history = model.fit_generator(
    train_it,
    epochs=params.epochs,
    steps_per_epoch=steps_per_epoch,
    validation_data=val_it,
    validation_steps=validation_steps,
    verbose=2,
    callbacks=[early_stopping_callback]
)
```

À l'exécution de l'entraînement, celui-ci peut s'interrompre de lui-même avant d'atteindre la limite d'epoch renseignée. Cet événement ce produit grâce à l'option "EarlyStopping" qui arrête le traitement dès lors où cela n'ai plus rentable (évite le sur-apprentissage). Durant la session d'apprentissage, celle-ci s'est arrêtée à l'époch 13 car la val_loss ne semblait plus pouvoir évoluer.

```
...
Epoch 11/15
 - 5s - loss: 0.1881 - accuracy: 0.9583 - val_loss: 1.0121 - val_accuracy: 0.8056
Epoch 12/15
 - 5s - loss: 0.1089 - accuracy: 0.9750 - val_loss: 0.5549 - val_accuracy: 0.8222
Epoch 13/15
 - 5s - loss: 0.0312 - accuracy: 0.9958 - val_loss: 0.0090 - val_accuracy: 0.8111
```

J'ai alors obtenu le graphique suivant :

![alt text](./images/results/cnn/model/cnn.png "Résultats entraînement")

#### B) Prédictions
Ci-dessous l'évaluation des prédictions du CNN :

```
"art_cybr" evaluation: {'TP': 0, 'FN': 30, 'FP': 0, 'accuracy': 0, 'recall': 0}
"art_dino" evaluation: {'TP': 40, 'FN': 0, 'FP': 18, 'accuracy': 0.6896551724137931, 'recall': 1.0}
"art_mural" evaluation: {'TP': 5, 'FN': 23, 'FP': 8, 'accuracy': 0.38461538461538464, 'recall': 0.17857142857142858}
"art_antiques" evaluation: {'TP': 15, 'FN': 15, 'FP': 3, 'accuracy': 0.8333333333333334, 'recall': 0.5}
"art_1" evaluation: {'TP': 25, 'FN': 5, 'FP': 44, 'accuracy': 0.36231884057971014, 'recall': 0.8333333333333334}
**average_accuracy: 0,453984546**
**average_recall: 0,502380952**
```

### Conclusion intermédiaire
Le CNN que j'ai développé semble étonnamment peu performant malgré un entraînement et une phase de validation avec une bonne précision. J'ai tout de même pu constater que ce soit au travers de mes recherches internet ainsi qu'auprès d'autres camarades que la précision obtenue est sensée être excellente, contrairement à la méthode (old school), le CNN semblerait mieux s'ajuster aux données, sa phase d'apprentissage lui permettrait d'offrir de meilleures prédictions.

## Conclusion générale
Pour conclure, avec la méthode "handcrafted features" (old school) il est nécessaire d'implémenter des algorithmes pour chaque caractéristique que l'on souhaite prendre en compte, et trouver la bonne proportion de mixage des probabilité qui en découle afin d'obtenir une bonne prédiction. Comparée au CNN, cette méthode manque de robustesse mais à un temps d'apprentissage beaucoup plus court.

La méthode moderne, le CNN, à l'inconvénient d'être long à entraîner, en revanche, dans l'usage, celui-ci se révèle beaucoup plus facile à mettre en place, rapide et robuste. 
 * "Facile à mettre en place" car sans grande connaissance du domaine de l'image, celui-ci permet d'obtenir de meilleurs résultats que la méthode "handcrafted features" grâce à un ensemble de mécanismes calculatoires.
 * "Rapide" car il n'a pas besoin de calculer à l'avance les attributs.
 * "robuste" car s'il est correctement entraîné, translation, rotation et changement d'échelle ne l'empêche pas de faire de bonnes prédictions.

En bref, le CNN possède une meilleure capacité de généralisation, mais représente également une perte de contrôle sur les tâches réalisées (boîte noire).
